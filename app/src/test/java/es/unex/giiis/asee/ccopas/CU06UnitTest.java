package es.unex.giiis.asee.ccopas;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.model.Review;

import static org.junit.Assert.assertEquals;

public class CU06UnitTest {

    @Test
    public void PonerResenaBebidaTest(){

        List<Review> bebidaReviews=new ArrayList<>();
        Date mockDate = new Date(2020,6, 20);
        Review mockReview = new Review(1, "BebidaMock","user", "title", "text", mockDate, 3);
        Bebida bebidaMock = new Bebida("BebidaMock","URL", Bebida.TipoBebida.Refresco,bebidaReviews);

        assertEquals(bebidaMock.getBebidaReviews().size(),0);
        bebidaMock.getBebidaReviews().add(mockReview);
        assertEquals(bebidaMock.getBebidaReviews().size(),1);
        assertEquals(bebidaMock.getBebidaReviews().get(0).getText(),"text");
        assertEquals(bebidaMock.getBebidaReviews().get(0).getTitle(),"title");
        assertEquals(bebidaMock.getBebidaReviews().get(0).getRate(),3.0,0);
        assertEquals(bebidaMock.getBebidaReviews().get(0).getReviewId(),1);
        assertEquals(bebidaMock.getBebidaReviews().get(0).getUser(),"user");
        assertEquals(bebidaMock.getBebidaReviews().get(0).getDate(),mockDate);



    }

}
