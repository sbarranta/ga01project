package es.unex.giiis.asee.ccopas;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.ccopas.uis.BarManagerActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isNotChecked;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AddBebidasFavTest {

    @Rule
    public ActivityTestRule<BarManagerActivity> mActivityTestRule = new ActivityTestRule<>(BarManagerActivity.class);

    @Test
    public void addBebidasFavTest() {
        //Bebida que vamos a poner como Fav
        String bebidaFav="'57 Chevy with a White License Plate";

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        //Pulsar en el primer elemento de la lista de bares
        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.bar_reclycler_view),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));

        recyclerView.perform(actionOnItemAtPosition(0, click()));

        //Ver si existe la tabs Carta
        ViewInteraction textView = onView(
                allOf(withText("CARTA"),
                        withParent(allOf(withContentDescription("Carta"),
                                withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class)))),
                        isDisplayed()));
        textView.check(matches(withText("CARTA")));

        //Hacer Click en carta
        ViewInteraction tabView = onView(
                allOf(withContentDescription("Carta"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.tabs),
                                        0),
                                1),
                        isDisplayed()));
        tabView.perform(click());

        //bebida que quiero añadir como favorita
        ViewInteraction textView2 = onView(
                allOf(withId(R.id.drink_nameTV), withText(bebidaFav),
                        withParent(withParent(IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class))),
                        isDisplayed()));
        textView2.check(matches(withText(bebidaFav)));

        ViewInteraction listaBebidasRV = onView(
                allOf(withId(R.id.listaCartaBarRV),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        listaBebidasRV.perform(actionOnItemAtPosition(0, click()));


        ViewInteraction marcarFavCheckBox = onView(
                allOf(withId(R.id.checkBoxBebidaFav), withText("Bebida Favorita"),
                        withParent(withParent(withId(R.id.view_pager_bebida))),
                        isDisplayed()));
        //Comprobar que NO esta marcado como favorito
        marcarFavCheckBox.check(matches(isNotChecked()));

        //Marcar como favorito
        marcarFavCheckBox.perform(click());

        pressBack();

        ViewInteraction recyclerView3 = onView(
                allOf(withId(R.id.listaCartaBarRV),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView3.perform(actionOnItemAtPosition(0, click()));

        //Comprobar que esta marcado como favorito
        marcarFavCheckBox.check(matches(isChecked()));

        pressBack();

        pressBack();

        //Abrir el panel lateral de navegación
        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Abriendo menú"),
                        childAtPosition(
                                allOf(withId(R.id.bar_manager_toolbar),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        //Pulsar en bebidas favoritas
        ViewInteraction bebidasFavItemView = onView(
                allOf(withId(R.id.nav_fav_drink),
                        childAtPosition(
                                allOf(withId(R.id.design_navigation_view),
                                        childAtPosition(
                                                withId(R.id.navigation_view),
                                                0)),
                                4),
                        isDisplayed()));
        bebidasFavItemView.perform(click());

        ViewInteraction textViewListBebidasFav = onView(
                allOf(withId(R.id.drink_nameTV), withText("'57 Chevy with a White License Plate"),
                        withParent(withParent(IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class))),
                        isDisplayed()));
        textViewListBebidasFav.check(matches(withText(bebidaFav)));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
