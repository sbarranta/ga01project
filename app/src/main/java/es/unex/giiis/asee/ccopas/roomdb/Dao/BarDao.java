package es.unex.giiis.asee.ccopas.roomdb.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.asee.ccopas.model.Bar;

import static androidx.room.OnConflictStrategy.IGNORE;
import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface BarDao {
    @Query("SELECT * FROM bares")
    public LiveData<List<Bar>> getAll();

    @Query("SELECT * FROM bares WHERE fav = 1")
    public LiveData<List<Bar>> getAllFav();

    @Query("SELECT * FROM bares WHERE nombre = :nombre")
    public Bar getBar(String nombre);

    @Insert(onConflict = IGNORE)
    public long insert(Bar bar);

    @Insert(onConflict = IGNORE)
    void bulkInsert(List<Bar> bar);

    @Query("DELETE FROM bares")
    public void deleteAll();

    @Update
    public int updateStatus(Bar bar);

}
