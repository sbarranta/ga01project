package es.unex.giiis.asee.ccopas.roomdb.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.asee.ccopas.model.Review;

@Dao
public interface ReviewDao {
    @Query("SELECT * FROM reviews")
    public List<Review> getAll();

    @Query("SELECT * FROM reviews WHERE nombreBar = :nombre AND tipo = :tipo")
    public LiveData<List<Review>> getAllReviewsByType(String nombre, Review.TipoReview tipo);

    @Insert
    public long insert(Review review);

    @Query("DELETE FROM reviews")
    public void deleteAll();

    @Update
    public int updateStatus(Review review);

}
