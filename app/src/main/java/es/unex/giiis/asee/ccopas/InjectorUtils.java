package es.unex.giiis.asee.ccopas;

import android.content.Context;

import es.unex.giiis.asee.ccopas.roomdb.BarDatabase;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BarListFragmentViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BarManagerViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BebidasFavoritasFragmentViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.CartaBarFragmentViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.FavBarListFragmentViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.InfoBarFragmentViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.PlaceholderBebidasFragmentViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.ReviewFragmentViewModelFactory;

public class InjectorUtils {
    public static BarRepository provideRepository(Context context){
        BarDatabase barDatabase = BarDatabase.getInstance(context.getApplicationContext());
        BarNetworkDataSource barNetworkDataSource = BarNetworkDataSource.getInstance();
        BebidaNetworkDataSource bebidaNetworkDataSource = BebidaNetworkDataSource.getInstance();
        return BarRepository.getInstance(barDatabase.getDao(),barNetworkDataSource,barDatabase.getBebidaDao(),bebidaNetworkDataSource,barDatabase.getReviewDao());
    }

    public static BarManagerViewModelFactory provideBarManagerActivityViewModelFactory(Context context){
        BarRepository barRepository = provideRepository(context);
        return new BarManagerViewModelFactory(barRepository);
    }

    public static BarListFragmentViewModelFactory provideBarListFragmentViewModelFactory(Context context){
        BarRepository barRepository = provideRepository(context);
        return  new BarListFragmentViewModelFactory(barRepository);
    }

    public static FavBarListFragmentViewModelFactory provideFavBarListFragmentViewModelFactory(Context context){
        BarRepository barRepository = provideRepository(context);
        return  new FavBarListFragmentViewModelFactory(barRepository);
    }

    public static BebidasFavoritasFragmentViewModelFactory provideBebidasFavoritasFragmentViewModelFactory(Context context){
        BarRepository barRepository = provideRepository(context);
        return new BebidasFavoritasFragmentViewModelFactory(barRepository);
    }

    public static InfoBarFragmentViewModelFactory provideInfoBarFragmentViewModelFactory(Context context){
        BarRepository barRepository = provideRepository(context);
        return  new InfoBarFragmentViewModelFactory(barRepository);
    }

    public static CartaBarFragmentViewModelFactory provideCartaBarFragmentViewModelFactory(Context context){
        BarRepository barRepository = provideRepository(context);
        return new CartaBarFragmentViewModelFactory(barRepository);
    }

    public static ReviewFragmentViewModelFactory provideReviewFragmentViewModelFactory(Context context,String nombre){
        BarRepository barRepository = provideRepository(context);
        return new ReviewFragmentViewModelFactory(barRepository,nombre);
    }

    public static PlaceholderBebidasFragmentViewModelFactory providePlaceholderBebidasFragmentViewModelFactory(Context context, String nombre){
        BarRepository barRepository = provideRepository(context);
        return new PlaceholderBebidasFragmentViewModelFactory(barRepository,nombre);
    }
}
