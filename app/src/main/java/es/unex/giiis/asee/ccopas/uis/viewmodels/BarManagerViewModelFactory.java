package es.unex.giiis.asee.ccopas.uis.viewmodels;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.ccopas.BarRepository;

public class BarManagerViewModelFactory  extends ViewModelProvider.NewInstanceFactory {
    private final BarRepository barRepository;


    public BarManagerViewModelFactory(BarRepository barRepository){
        this.barRepository=barRepository;
    }
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass){
        return (T) new BarManagerActivityViewModel(barRepository);
    }
}
