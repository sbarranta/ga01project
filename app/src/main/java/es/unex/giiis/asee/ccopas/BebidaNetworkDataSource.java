package es.unex.giiis.asee.ccopas;

import android.widget.ArrayAdapter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import es.unex.giiis.asee.ccopas.API.BebidaNetworkLoaderRunnable;
import es.unex.giiis.asee.ccopas.Executors.AppExecutors;
import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.model.cocktailAPI.Drink;

public class BebidaNetworkDataSource {
    private static BebidaNetworkDataSource mInstance;

    private final MutableLiveData<ArrayList<Bebida>> mDownloadedBebidas = new MutableLiveData<ArrayList<Bebida>>();

    public synchronized  static BebidaNetworkDataSource getInstance(){
        if(mInstance ==null){
            mInstance = new BebidaNetworkDataSource();
        }
        return mInstance;
    }
    public LiveData<ArrayList<Bebida>> getCurrentBebida(){return mDownloadedBebidas;}
    public void fetchBebidas(){
        AppExecutors.getInstance().networkIO().execute(new BebidaNetworkLoaderRunnable((cocktailDB -> {
            ArrayList<Bebida> bebidas = new ArrayList<>();
            Bebida bebida;
            for(Drink drink : cocktailDB.getDrinks()){
                bebida = new Bebida(drink.getStrDrink(),drink.getStrDrinkThumb());
                bebidas.add(bebida);
            }
            System.out.println(bebidas.size());
            mDownloadedBebidas.postValue(bebidas);
        })));
    }
}
