package es.unex.giiis.asee.ccopas.roomdb.Converter;

import androidx.room.TypeConverter;

import es.unex.giiis.asee.ccopas.model.Review;

public class TipoReviewConverter {
    @TypeConverter
    public String tipoReviewToString(Review.TipoReview tipo){
        return tipo == null ? null : tipo.name();
    }
    @TypeConverter
    public Review.TipoReview stringTotipoReview (String tipo){
        return  tipo == null ? null : Review.TipoReview.valueOf(tipo);
    }
}
