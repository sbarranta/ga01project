package es.unex.giiis.asee.ccopas.roomdb;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.model.Review;
import es.unex.giiis.asee.ccopas.roomdb.Converter.BebidasListConverter;
import es.unex.giiis.asee.ccopas.roomdb.Converter.DateConverter;
import es.unex.giiis.asee.ccopas.roomdb.Converter.ReviewListConverter;
import es.unex.giiis.asee.ccopas.roomdb.Converter.TipoBebidaConverter;
import es.unex.giiis.asee.ccopas.roomdb.Converter.TipoReviewConverter;
import es.unex.giiis.asee.ccopas.roomdb.Dao.BarDao;
import es.unex.giiis.asee.ccopas.roomdb.Dao.BebidaDao;
import es.unex.giiis.asee.ccopas.roomdb.Dao.ReviewDao;

@Database(entities = {Bar.class, Review.class, Bebida.class}, version = 1)
@TypeConverters({ReviewListConverter.class, DateConverter.class, BebidasListConverter.class, TipoBebidaConverter.class, TipoReviewConverter.class})
public abstract class BarDatabase extends RoomDatabase {
    private static BarDatabase instance;

    public static BarDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, BarDatabase.class, "Bar.db").build();
        }
        return instance;
    }

    public abstract BarDao getDao();
    public abstract ReviewDao getReviewDao();
    public abstract BebidaDao getBebidaDao();
}