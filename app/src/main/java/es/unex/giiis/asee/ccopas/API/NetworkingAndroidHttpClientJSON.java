package es.unex.giiis.asee.ccopas.API;

import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.ccopas.model.Bebida;


public class NetworkingAndroidHttpClientJSON {

    private List<Bebida> drinkList;

    //Variables para thecocktaildb

    private static final String BASE_URLdrink = "www.thecocktaildb.com";
    private static final String path = "api";
    private static final String path2 = "json";
    private static final String path3 = "v1";
    private static final String path4 = "1";
    private static final String path5 = "filter.php";
    private static final String param = "Alcoholic";


    /*
     * public builder
     * Initialize product lists
     * Ejecutar con el hilo de NetworkIO los metodos que obtienen los productos de la API
     */
    public NetworkingAndroidHttpClientJSON() {

        drinkList= new ArrayList<>();
    }

    /*
     * GET the drinks list
     *
     * @return List containing all the drinks
     */
    public List<Bebida> getListDrinks() {
        return drinkList;
    }


    /*
     * Cargar las bebidas *****************************************************************
     * */


    /*
     * Construir la utl para la API thecocktaildb para sacar las bebidas
     * */
    public void loadDrink() {
            URL queryURL;
            JSONObject result;

            queryURL = NetworkUtils.buildURL(BASE_URLdrink,
                    new String[]{path, path2, path3, path4, path5},
                    new Pair<>("a", param));
            result = NetworkUtils.getJSONResponse(queryURL);

            if (result != null) {
                jsonToListDrink(result);
            }

    }

    /*
     * Recibe un JSON y se va recorriendo, y se extrae los productos
     * que se usaran para rellenar la lista de bebidas
     * */
    private void jsonToListDrink(JSONObject responseObject) {

        List<Bebida> drinkList = new ArrayList<>();
        String nameDrink;
        String urlImageDrink;
        JSONArray Drinks;
        try {

            Drinks = responseObject.getJSONArray("drinks");
            for (int idx = 0; idx < 30; idx++) {
                // Get single Product data - a Map
                JSONObject Product = (JSONObject) Drinks.get(idx);
                nameDrink = Product.get("strDrink").toString();
                urlImageDrink = Product.get("strDrinkThumb").toString();
                Bebida ProductObj = new Bebida(nameDrink,urlImageDrink);

                this.drinkList.add(ProductObj);//Añade la bebida

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

