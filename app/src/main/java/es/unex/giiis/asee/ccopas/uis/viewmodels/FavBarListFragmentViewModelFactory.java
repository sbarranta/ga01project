package es.unex.giiis.asee.ccopas.uis.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.ccopas.BarRepository;

public class FavBarListFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final BarRepository barRepository;

    public FavBarListFragmentViewModelFactory(BarRepository barRepository){
        this.barRepository = barRepository;
    }

    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new FavBarListFragmentViewModel(barRepository);
    }
}
